FROM docker-registry.wikimedia.org/envoy:latest 

COPY testdata/sophroid-envoy.yaml /etc/envoy/envoy.yaml

EXPOSE 9003