# Sophroid - xDS Service for WMF

Introducing: Sophroid - the control plane for our service mesh.   

## Run locally 

To run the xDS server
```bash
make run-server
```

To run envoy in docker
```bash
make setup
make launch
``` 

## Description, Project Status, Next Steps 

In progress!