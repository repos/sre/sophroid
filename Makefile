.PHONY: run-server static-envoy-start sophroid-envoy-start setup-demo launch-demo

run-server:
	go run cmd/sophroid/main.go 
	
# -skipClusters=local_service,otel_collector,admin_interface

static-envoy-start:
	envoy -c hack/mw-debug-envoy.yaml

sophroid-envoy-start: 
	make run-server & envoy -c hack/mw-debug-envoy-dynamic.yaml

setup: 
	docker build . -t sophroid-demo	
launch:
	docker run --network=host sophroid-demo


