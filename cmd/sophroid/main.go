package main

import (
	"context"
	"flag"

	"github.com/envoyproxy/go-control-plane/pkg/cache/v3"
	serverv3 "github.com/envoyproxy/go-control-plane/pkg/server/v3"
	"github.com/envoyproxy/go-control-plane/pkg/test/v3"
	"gitlab.wikimedia.org/repos/sre/sophroid/internal/processor"
	server "gitlab.wikimedia.org/repos/sre/sophroid/internal/server"
	"gitlab.wikimedia.org/repos/sre/sophroid/internal/watcher"

	log "github.com/sirupsen/logrus"
)

var (
	l             log.FieldLogger
	port          uint
	nodeID        string
	watchFileName string
	//skipClusterNames string
)

func init() {
	l = log.New()
	log.SetLevel(log.TraceLevel)

	// the port that this xDS server listens on
	flag.UintVar(&port, "port", 18000, "xDS management server port")

	// to do: upgrade from simple to linear cache such that sophroid responds to multiple envoys with unique nodeIDs
	// currently the nodeID is hardcoded to docker-desktop for testing purposes
	flag.StringVar(&nodeID, "nodeID", "docker-desktop", "Node ID")

	// File to watch for service configuration changes
	flag.StringVar(&watchFileName, "watchFileName", "config/config.yaml", "full path to file to watch for changes")

	// to do: list of clusters to skip, taken as a command line flag
	// flag.StringVar(&skipClusterNames, "skipClusters", "", "list of clusters to skip, seperated by comma")

}

func main() {

	flag.Parse()

	// Create a cache
	cache := cache.NewSnapshotCache(false, cache.IDHash{}, l)

	proc := processor.NewProcessor(
		cache, nodeID, log.WithField("context", "processor"))
	// Create initial snapshot from file
	proc.ProcessFile(watcher.NotifyMessage{
		Operation: watcher.Create,
		FilePath:  watchFileName,
	})

	// Notify channel for file system events
	notifyCh := make(chan watcher.NotifyMessage)

	go func() {
		// Watch for file changes
		watcher.Watch(watchFileName, notifyCh)
	}()

	go func() {
		// Run the xDS server
		ctx := context.Background()
		cb := &test.Callbacks{Debug: true}
		srv := serverv3.NewServer(ctx, cache, cb)
		server.RunServer(ctx, srv, port)
	}()

	for {
		select {
		case msg := <-notifyCh:
			proc.ProcessFile(msg)
		}

	}
}
