package xdscache

import (
	clusterv3 "github.com/envoyproxy/go-control-plane/envoy/config/cluster/v3"
	listenerv3 "github.com/envoyproxy/go-control-plane/envoy/config/listener/v3"
	"github.com/envoyproxy/go-control-plane/pkg/cache/types"
)

type XDSCache struct {
	Listeners map[string]*listenerv3.Listener
	Clusters  map[string]*clusterv3.Cluster
}

// Retrieves cluster resources from the cache
func (xds *XDSCache) ClusterContents() []types.Resource {
	var r []types.Resource
	for _, c := range xds.Clusters {
		r = append(r, c)
	}
	return r
}

// Retrieves listener resources from the cache
func (xds *XDSCache) ListenerContents() []types.Resource {
	var r []types.Resource
	for _, l := range xds.Listeners {
		r = append(r, l)
	}
	return r
}

// Adds a cluster to the cache
func (xds *XDSCache) AddCluster(cluster *clusterv3.Cluster) {
	xds.Clusters[cluster.GetName()] = cluster
}

// Adds a listener to the cache
func (xds *XDSCache) AddListener(listener *listenerv3.Listener) {
	xds.Listeners[listener.GetName()] = listener
}
