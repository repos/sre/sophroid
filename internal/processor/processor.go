package processor

import (
	"context"
	"fmt"
	"math"
	"math/rand"
	"os"
	"strconv"
	"strings"

	clusterv3 "github.com/envoyproxy/go-control-plane/envoy/config/cluster/v3"
	listenerv3 "github.com/envoyproxy/go-control-plane/envoy/config/listener/v3"
	"gitlab.wikimedia.org/repos/sre/sophroid/internal/watcher"
	"gitlab.wikimedia.org/repos/sre/sophroid/internal/xdscache"

	bootstrap "github.com/envoyproxy/go-control-plane/envoy/config/bootstrap/v3"
	_ "github.com/envoyproxy/go-control-plane/envoy/extensions/access_loggers/file/v3"
	_ "github.com/envoyproxy/go-control-plane/envoy/extensions/filters/http/router/v3"
	_ "github.com/envoyproxy/go-control-plane/envoy/extensions/filters/listener/tls_inspector/v3"
	_ "github.com/envoyproxy/go-control-plane/envoy/extensions/upstreams/http/v3"
	yaml "sigs.k8s.io/yaml"

	"github.com/envoyproxy/go-control-plane/pkg/cache/types"
	"github.com/envoyproxy/go-control-plane/pkg/cache/v3"
	"github.com/envoyproxy/go-control-plane/pkg/resource/v3"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"
)

type Processor struct {
	cache  cache.SnapshotCache
	nodeID string
	// snapshotVersion holds the current version of the snapshot.
	snapshotVersion int64
	logrus.FieldLogger
	xdsCache xdscache.XDSCache
	//skipClusterNames string
}

// parseYaml reads a YAML file and unmarshals it into a Bootstrap configuration.
func parseYaml(fileName string) (*bootstrap.Bootstrap, error) {
	var config bootstrap.Bootstrap
	yamlFile, err := os.ReadFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("error reading YAML file: %s", err)
	}
	// converting yaml to json before unmarshalling to make use of json struct tags in protobuf
	configJSON, err := yaml.YAMLToJSON(yamlFile)
	if err != nil {
		return nil, fmt.Errorf("error converting YAML to JSON: %s", err)
	}

	err = protojson.Unmarshal(configJSON, &config)
	if err != nil {
		return nil, fmt.Errorf("error unmarshaling YAML file: %s", err)

	}
	return &config, nil
}

func NewProcessor(cache cache.SnapshotCache, nodeID string, log logrus.FieldLogger) *Processor {
	return &Processor{
		cache:           cache,
		nodeID:          nodeID,
		snapshotVersion: rand.Int63n(1000), // initalized starting at a random snapshot version number to avoid conflicts
		FieldLogger:     log,
		xdsCache: xdscache.XDSCache{
			Listeners: make(map[string]*listenerv3.Listener),
			Clusters:  make(map[string]*clusterv3.Cluster),
		},
	}
}

// newSnapshotVersion increments the current snapshotVersion
// and returns as a string.
func (p *Processor) newSnapshotVersion() string {

	// Reset the snapshotVersion if it ever hits max size.
	if p.snapshotVersion == math.MaxInt64 {
		p.snapshotVersion = 0
	}

	// Increment the snapshot version & return as string.
	p.snapshotVersion++
	return strconv.FormatInt(p.snapshotVersion, 10)
}

// ProcessFile watches a file and generates an xDS snapshot
func (p *Processor) ProcessFile(file watcher.NotifyMessage) {

	// Parse the YAML file
	envoyConfig, err := parseYaml(file.FilePath)

	if err != nil {
		p.Errorf("error parsing YAML file: %s", err)
		return
	}

	// skip parsing clusters with names in skipClusters (for example: local_service, otel_collector or admin_interface)
	// which we do not want to pass to envoy through sophroids xDS service, rather we'll include them in the config sent to envoy directly
	// + skip parsing listeners with ports 4444 and 9361 since these are local service and admin interface respectively
	// to-do: we will eventually take these as command line flags skipClusterNames and skipPorts respectively
	skipClusters := map[string]struct{}{
		"local_service":   {},
		"otel_collector":  {},
		"admin_interface": {},
	}
	skipPorts := map[uint32]struct{}{
		4444: {},
		9361: {},
	}

	// to-do: populating skipClusters as input from a command line flag, rather than hardcoding
	/*
		skipClusters := make(map[string]struct{})
		for _, cluster := range strings.Split(skipClusterNames, ",") {
			skipClusters[cluster] = struct{}{}
		} */

	clusters := envoyConfig.GetStaticResources().GetClusters()
	for _, cluster := range clusters {
		clusterName := cluster.GetName()
		// skip parsing clusters with names in skipClusters
		if _, ok := skipClusters[clusterName]; ok {
			continue
		}
		// Add each cluster to the cache
		p.xdsCache.AddCluster(cluster)
	}

	listeners := envoyConfig.GetStaticResources().GetListeners()
	for _, listener := range listeners {
		// skip parsing listeners with ports in skipPorts
		if _, ok := skipPorts[listener.GetAddress().GetSocketAddress().GetPortValue()]; ok {
			continue
		}
		//if name is nil, replace with listener + portvalue + v4/v6 based on address
		if listener.GetName() == "" {
			//check if listener address contains ":"", to distinguish between v4 and v6
			if strings.Contains(listener.GetAddress().GetSocketAddress().GetAddress(), ":") {
				// if address contains ":", we append "v6" to the listener name
				listener.Name = "listener" + strconv.Itoa(int(listener.GetAddress().GetSocketAddress().GetPortValue())) + "v6"
			} else {
				// otherwise we append "v4" to the listener name
				listener.Name = "listener" + strconv.Itoa(int(listener.GetAddress().GetSocketAddress().GetPortValue())) + "v4"
			}
			// Add each listener to the cache
			p.xdsCache.AddListener(listener)
		}

		// Create a resource map keyed off the type URL of a resource,
		// followed by the slice of resource objects.
		resources := map[resource.Type][]types.Resource{
			resource.ClusterType:  p.xdsCache.ClusterContents(),
			resource.ListenerType: p.xdsCache.ListenerContents(),
		}

		// Create the snapshot that we'll serve to Envoy
		snapshot, err := cache.NewSnapshot(
			p.newSnapshotVersion(), // version
			resources,
		)
		if err != nil {
			p.Errorf("error generating new snapshot: %v", err)
			return
		}

		if err := snapshot.Consistent(); err != nil {
			p.Errorf("snapshot inconsistency: %+v\n\n\n%+v", snapshot, err)
			return
		}
		p.Debugf("will serve snapshot %+v", snapshot)

		// Add the snapshot to the cache
		if err := p.cache.SetSnapshot(context.Background(), p.nodeID, snapshot); err != nil {
			p.Fatalf("snapshot error %q for %+v", err, snapshot)
		}

	}
}
